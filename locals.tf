locals {
  # IANA protocol numbers
  protocols = {
    icmp = 1
    tcp  = 6
    udp  = 17
  }
}
